directory "/srv/www/" do
  mode 0755
  owner 'root'
  group 'root'
  action :create
end